/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.3520408163265306, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "Login"], "isController": false}, {"data": [0.0, 500, 1500, "GET Buyer Product"], "isController": false}, {"data": [0.6428571428571429, 500, 1500, "POST Buyer Order"], "isController": false}, {"data": [0.42857142857142855, 500, 1500, "GET Buyer Order"], "isController": false}, {"data": [0.2857142857142857, 500, 1500, "POST  Seller Product"], "isController": false}, {"data": [0.2857142857142857, 500, 1500, "GET  Seller Product ID"], "isController": false}, {"data": [0.0, 500, 1500, "GET Buyer Product id"], "isController": false}, {"data": [0.5, 500, 1500, "GET Buyer Order ID"], "isController": false}, {"data": [0.6428571428571429, 500, 1500, "DELETE Seller Product ID"], "isController": false}, {"data": [0.0, 500, 1500, "Register"], "isController": false}, {"data": [0.2857142857142857, 500, 1500, "GET  Seller Product"], "isController": false}, {"data": [0.5714285714285714, 500, 1500, "PUT Buyer Order ID"], "isController": false}, {"data": [0.42857142857142855, 500, 1500, "Login Buyer"], "isController": false}, {"data": [0.8571428571428571, 500, 1500, "DELETE Buyer Order ID"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 98, 0, 0.0, 9190.62244897959, 275, 99131, 1481.0, 40759.10000000006, 60632.69999999999, 99131.0, 0.5943212002862445, 421.0732124216921, 0.017743406370152946], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Login", 7, 0, 0.0, 3881.4285714285716, 1819, 4859, 4450.0, 4859.0, 4859.0, 4859.0, 1.41643059490085, 0.7123649964589235, 0.0], "isController": false}, {"data": ["GET Buyer Product", 7, 0, 0.0, 69363.14285714286, 13377, 99131, 64902.0, 99131.0, 99131.0, 99131.0, 0.06475365858170987, 319.1131480569277, 0.0], "isController": false}, {"data": ["POST Buyer Order", 7, 0, 0.0, 602.0, 355, 858, 568.0, 858.0, 858.0, 858.0, 0.055831611858634354, 0.03751186421751996, 0.0], "isController": false}, {"data": ["GET Buyer Order", 7, 0, 0.0, 1516.5714285714284, 333, 4252, 781.0, 4252.0, 4252.0, 4252.0, 0.055923943436925784, 3.1513937919429575, 0.0], "isController": false}, {"data": ["POST  Seller Product", 7, 0, 0.0, 2414.142857142857, 413, 3893, 3449.0, 3893.0, 3893.0, 3893.0, 1.0649627263045793, 0.7217618477103301, 0.0], "isController": false}, {"data": ["GET  Seller Product ID", 7, 0, 0.0, 1458.4285714285713, 275, 2638, 1830.0, 2638.0, 2638.0, 2638.0, 0.7839623698062493, 0.5703632475641169, 0.0], "isController": false}, {"data": ["GET Buyer Product id", 7, 0, 0.0, 40056.57142857143, 16734, 54994, 40075.0, 54994.0, 54994.0, 54994.0, 0.04942455694415025, 243.54088337569723, 0.0], "isController": false}, {"data": ["GET Buyer Order ID", 7, 0, 0.0, 1228.2857142857142, 385, 3666, 867.0, 3666.0, 3666.0, 3666.0, 0.056134273181450034, 0.06907146895373735, 0.0], "isController": false}, {"data": ["DELETE Seller Product ID", 7, 0, 0.0, 735.5714285714286, 314, 1590, 557.0, 1590.0, 1590.0, 1590.0, 0.05759562931452973, 0.018111125624295482, 0.0], "isController": false}, {"data": ["Register", 7, 0, 0.0, 2783.9999999999995, 2056, 3495, 2761.0, 3495.0, 3495.0, 3495.0, 1.9199122325836533, 1.115574002331322, 0.8024633159626988], "isController": false}, {"data": ["GET  Seller Product", 7, 0, 0.0, 1874.4285714285713, 591, 3578, 1484.0, 3578.0, 3578.0, 3578.0, 0.8087810514153668, 0.589999458405546, 0.0], "isController": false}, {"data": ["PUT Buyer Order ID", 7, 0, 0.0, 1043.2857142857142, 287, 2509, 761.0, 2509.0, 2509.0, 2509.0, 0.05639703512729617, 0.03750623136883661, 0.0], "isController": false}, {"data": ["Login Buyer", 7, 0, 0.0, 1313.7142857142858, 459, 2562, 1136.0, 2562.0, 2562.0, 2562.0, 0.6441520198766909, 0.3277375023005429, 0.0], "isController": false}, {"data": ["DELETE Buyer Order ID", 7, 0, 0.0, 397.14285714285717, 283, 723, 299.0, 723.0, 723.0, 723.0, 0.05742553139125655, 0.01850627476476041, 0.0], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 98, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
