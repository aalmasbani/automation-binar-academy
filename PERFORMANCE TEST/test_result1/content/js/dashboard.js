/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.42857142857142855, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "Login"], "isController": false}, {"data": [0.0, 500, 1500, "GET Buyer Product"], "isController": false}, {"data": [0.6, 500, 1500, "POST Buyer Order"], "isController": false}, {"data": [0.6, 500, 1500, "GET Buyer Order"], "isController": false}, {"data": [0.5, 500, 1500, "POST  Seller Product"], "isController": false}, {"data": [0.3, 500, 1500, "GET  Seller Product ID"], "isController": false}, {"data": [0.0, 500, 1500, "GET Buyer Product id"], "isController": false}, {"data": [0.8, 500, 1500, "GET Buyer Order ID"], "isController": false}, {"data": [0.7, 500, 1500, "DELETE Seller Product ID"], "isController": false}, {"data": [0.0, 500, 1500, "Register"], "isController": false}, {"data": [0.4, 500, 1500, "GET  Seller Product"], "isController": false}, {"data": [0.8, 500, 1500, "PUT Buyer Order ID"], "isController": false}, {"data": [0.5, 500, 1500, "Login Buyer"], "isController": false}, {"data": [0.8, 500, 1500, "DELETE Buyer Order ID"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 70, 0, 0.0, 10126.914285714287, 279, 96216, 935.0, 54457.299999999996, 71689.80000000002, 96216.0, 0.40178853295526945, 284.6291267628472, 0.011995360777403413], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Login", 5, 0, 0.0, 2947.6, 1753, 4723, 1956.0, 4723.0, 4723.0, 4723.0, 1.0403662089055348, 0.5232310523304203, 0.0], "isController": false}, {"data": ["GET Buyer Product", 5, 0, 0.0, 77036.8, 54672, 96216, 75487.0, 96216.0, 96216.0, 96216.0, 0.04977898132292621, 245.26352992712356, 0.0], "isController": false}, {"data": ["POST Buyer Order", 5, 0, 0.0, 1912.8, 290, 5522, 348.0, 5522.0, 5522.0, 5522.0, 0.0530453325411897, 0.03563983280111183, 0.0], "isController": false}, {"data": ["GET Buyer Order", 5, 0, 0.0, 2297.6, 309, 8991, 733.0, 8991.0, 8991.0, 8991.0, 0.05465854805032959, 3.075589526602315, 0.0], "isController": false}, {"data": ["POST  Seller Product", 5, 0, 0.0, 1821.2, 440, 3994, 638.0, 3994.0, 3994.0, 3994.0, 0.7523322299127294, 0.50988141363226, 0.0], "isController": false}, {"data": ["GET  Seller Product ID", 5, 0, 0.0, 1717.2, 620, 4605, 743.0, 4605.0, 4605.0, 4605.0, 0.5690871841566129, 0.41403315644206695, 0.0], "isController": false}, {"data": ["GET Buyer Product id", 5, 0, 0.0, 46783.4, 15713, 67344, 52525.0, 67344.0, 67344.0, 67344.0, 0.04561045026636503, 224.74200163741517, 0.0], "isController": false}, {"data": ["GET Buyer Order ID", 5, 0, 0.0, 514.0, 284, 1000, 354.0, 1000.0, 1000.0, 1000.0, 0.06011493976483035, 0.0739695547887561, 0.0], "isController": false}, {"data": ["DELETE Seller Product ID", 5, 0, 0.0, 813.8, 292, 1837, 472.0, 1837.0, 1837.0, 1837.0, 0.06154452130671327, 0.01935286705152507, 0.0], "isController": false}, {"data": ["Register", 5, 0, 0.0, 2831.8, 2191, 3495, 2825.0, 3495.0, 3495.0, 3495.0, 1.3713658804168953, 0.7968385730938015, 0.5731880828304992], "isController": false}, {"data": ["GET  Seller Product", 5, 0, 0.0, 1143.2, 588, 2628, 594.0, 2628.0, 2628.0, 2628.0, 0.5790387955993052, 0.4224042776491025, 0.0], "isController": false}, {"data": ["PUT Buyer Order ID", 5, 0, 0.0, 438.0, 279, 590, 445.0, 590.0, 590.0, 590.0, 0.06064134284189589, 0.04032886179231553, 0.0], "isController": false}, {"data": ["Login Buyer", 5, 0, 0.0, 835.0, 407, 1783, 656.0, 1783.0, 1783.0, 1783.0, 0.502815768302494, 0.2558271633648431, 0.0], "isController": false}, {"data": ["DELETE Buyer Order ID", 5, 0, 0.0, 684.4, 343, 1404, 458.0, 1404.0, 1404.0, 1404.0, 0.06071350511207713, 0.01956587567088423, 0.0], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 70, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
